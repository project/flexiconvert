
Introduction
-------------------------------------
	The original module was written by starbow and was a quick way to convert 
old Flexinode types in Drupal 4.6 to CCK types in Drupal 5.x.  Recently this project 
has been updated for later Drupal 5.x releases.


Limitations and notices
-------------------------------------
    * May have trouble on highly customized fields and content types.
    * The customized settings for fields will not be carried over-- you will need to reset those yourself.
    * For "image" content types, the image source path and the gallery will not show up.
    * Content types from Flexinode are converted by their Flexinode name. If one or more of your Flexinode types has
      white space in it, the white space will be converted to underscores when the conversion finishes. For example the
      type "my example type" will be converted to "my_example_type". All amounts of white space between words will be 
      converted to a single underscore, however, the content name will still show the spaces.


Steps for use
-------------------------------------
   1. Make a complete back-up of your database before continuing.
   2. Make sure that the users who created the content still exist, or set the UID for all nodes to the UID of a user that does exist.
      If you have content that was created by a user that no longer exists, the content will be converted but will not appear at all.
   3. Make sure all your flexinode fields are unique.
   4. Turn off the Flexinode module-- you're probably not going to need it any more.
   5. Turn on the CCK & all of the content type modules that came with it, like Text, Select, et cetera.
   6. Go to admin/flexiconvert/convert and select all the node types you'd like to convert and then click "Convert". Alternatively,
      if you'd like to convert them all, check "Check all" and all of the types listed will be check marked for you.
   7. The "type" field in the {node} table will be updated to reflect the new types. This is necessary because Flexinode 
      uses an non-traditional method of type-naming it's content in the {node} table.


A note on white space
-------------------------------------
	White space in old Flexinode content names (not content types) was a legitimate idea when Flexinode was in use.  However, part of the conversion 
process of Flexinode types to CCK types involves finding a machine-readable type name for the Flexinode type to be converted.  The best solution is to use the 
old Flexinode content name.   Names can have white space but machine-readable types can not have white space.  This is a problem, but the solution is simple: 
convert all amounts of white space between words to a single underscore.

Known Bugs
-------------------------------------
  1. One major bug is that it will take custom fields and insert all of their content in to the body instead of 
  creating CCK fields for them.

Support
-------------------------------------
	If you find a bug, please submit a bug report to the project's page.
		http://drupal.org/project/flexiconvert

Credits
-------------------------------------
starbow
aj045
